<div class="content-wrapper pt5 pb5">
  <h2 class="title-intro title-before text-center mb4">Sign up for special deals</h2>
  <div class="form-content form-newsletter">
    <input class="text" type="text" placeholder="your@email.com">
    <button class="submit">Send</button>
    <p class=" text-center pt1 fsz14">Your email wont be used for spam. If we will spam you, youll get a free shave. We promise.</p>
  </div>
</div>
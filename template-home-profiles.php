<?php 
  $args = array(
    'post_type' => 'people'
  );

  $the_query = new WP_Query($args);
  if ( $the_query->have_posts() ) :
?>
<div class="content-wrapper pt5">
  <h2 class="title-intro title-before text-center mb4">Who will do the work</h2>
  <div class="flex-profiles flex-3 flex-gap-4">
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
    <div class="item">
      <div class="el-image">
        <?php 
          $image = get_field( 'image' );
        ?>
        <img src="<?php echo $image['url']; ?>" alt="">  
      </div>
      <div class="el-description">
        <h3 class="pt2 alternate"><?php the_title(); ?></h3>
        <div class="entry-content pt1">
          <?php echo apply_filters( 'the_content', get_field( 'bio' ) ); ?>
        </div>
        <img src="<?php bloginfo('template_url') ?>/images/icn_cross.svg" alt="">
        <p class="pt1">
          <i>Years of experience</i>
          <br><?php echo get_field( 'years_of_experience' ); ?>
        </p>
        <br>
        <p>
          <i>Contact:</i><br>
          <a class="email" href="mailto:<?php antispambot( get_field('email'), 1 ); ?>">
            <?php echo antispambot( get_field('email'), 0 ); ?>
          </a>
        </p>
      </div>
    </div>
    <?php endwhile; ?>
  </div>
</div>
<?php 

  endif;
  wp_reset_postdata();

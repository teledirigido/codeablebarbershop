<?php

include 'assets/scripts.php';
include 'assets/wp_footer.php';

// ACF
include 'acf/og.php';
include 'acf/options_page.php';
include 'acf/googlemaps.php';

// Registering Post Types
include 'assets/post_type.php';

// Misc Functions
include 'assets/misc.php';
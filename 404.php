<?php 
	get_header(); 
	the_post();
?>
  
  <div class="hero-wrapper">
    <div class="hero-content white">
      <div class="content-750 pt4">
        <h2 class="title-intro">The page you're looking for <br>is not ready yet</h2>
      </div>
    </div>
  </div>

<?php get_footer(); ?>
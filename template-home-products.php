<?php 
  $args = array(
    'post_type' => 'products'
  );

  $the_query = new WP_Query($args);
  if ( $the_query->have_posts() ) :
?>

<div class="content-wrapper pt5 pb5">
  <h2 class="title-intro title-before text-center mb4">Spend Some Money</h2>
  <div class="flex-product-grid flex-4 flex-gap-2">
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
    <div class="item item-product">
      <div class="el-image">
        <?php 
          $image = get_field('featured_image');
          $size = 'full';
          if( $image ):
            echo wp_get_attachment_image( $image['ID'], $size );
          endif;
        ?>
      </div>
      <div class="el-title">
        <h3 class="alternate pt1"><?php the_title(); ?></h3>
      </div>
      <div class="el-description">
        <div class="entry-content pt2">
          <?php the_content(); ?>
        </div>
        <div class="flex-2 pt1">
          <div class="item item-price"><i><?php echo '$' . get_field( 'price' ); ?></i></div>
          <div class="item item-action text-right"><a href="#" class="btn btn-transparent-black">Reserve</a></div>
        </div>
      </div>
    </div>
    <?php endwhile; ?>
  </div>
</div>
<?php 
  endif;
  wp_reset_postdata();

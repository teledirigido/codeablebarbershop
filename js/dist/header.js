'use strict';

console.log('hello from header.js!');
var headerMenu = {
  init: function init() {
    var headerTrigger = document.querySelector('.header-mobile-trigger');
    headerTrigger.addEventListener('click', this.triggerOnClick);
  },
  triggerOnClick: function triggerOnClick() {
    var headerWrapper = document.querySelector('.header-wrapper');
    var headerTrigger = document.querySelector('.header-mobile-trigger');
    if (!headerWrapper.classList.contains('menu-active')) {
      headerWrapper.classList.add('menu-active');
      headerTrigger.classList.add('active');
    } else {
      headerWrapper.classList.remove('menu-active');
      headerTrigger.classList.remove('active');
    }
  }
};

headerMenu.init();
//# sourceMappingURL=header.js.map

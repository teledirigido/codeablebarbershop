console.log('hello from header.js!')
var headerMenu = {
  init(){
    const headerTrigger = document.querySelector('.header-mobile-trigger')
    headerTrigger.addEventListener('click', this.triggerOnClick )
  },

  triggerOnClick(){
    const headerWrapper = document.querySelector('.header-wrapper')
    const headerTrigger = document.querySelector('.header-mobile-trigger')
    if( !headerWrapper.classList.contains('menu-active') ){
      headerWrapper.classList.add('menu-active')
      headerTrigger.classList.add('active')
    } else {
      headerWrapper.classList.remove('menu-active')
      headerTrigger.classList.remove('active')
    }
  }
  
}


headerMenu.init()
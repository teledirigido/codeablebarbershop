    <div class="footer-wrapper">
      <div class="content-wrapper flex-footer pt3 pb5">
        <div class="item item-logo flex-logo">
          <div class="item item-logo-image">
            <img src="<?php bloginfo('template_url') ?>/images/footer-logo.svg" alt="">
          </div>
          <div class="item">
            <h4 class="heading"><i>Contact</i></h4>
            <p class="white pt1">
              Rough Hands Barber Shop <br>
              8 Walton City <br>
              Imperial Beach <br>
              WA 57431
            </p>
            <p class="white pt1">
              7-(145)210-7757
              heyguys@rough-hands.com
            </p>
          </div>
        </div>
        <div class="item item-nav">
          <div class="flex-3">
            
            <div class="item">
              <h4 class="heading">Page:</h4>
              <ul class="white ul-vert">
                <li class="mt1"><a href="#">Home</a></li>
                <li class="mt1"><a href="#">About</a></li>
                <li class="mt1"><a href="#">Store</a></li>
                <li class="mt1"><a href="#">Contact</a></li>
              </ul>
            </div>

            <div class="item">
              <h4 class="heading">Various:</h4>
              <ul class="white ul-vert">
                <li class="mt1"><a href="#">FAQ</a></li>
                <li class="mt1"><a href="#">Legal</a></li>
                <li class="mt1"><a href="#">Pricing</a></li>
              </ul>
            </div>

            <div class="item">
              <h4 class="heading">Social:</h4>
              <ul class="white ul-vert">
                <li class="mt1"><a href="#">Facebook</a></li>
                <li class="mt1"><a href="#">Instagram</a></li>
                <li class="mt1"><a href="#">Pinterest</a></li>
                <li class="mt1"><a href="#">Grease Monkeys</a></li>
              </ul>
            </div>

          </div>
        </div>
      </div>      
    </div>
    <?php wp_footer(); ?>
    <script>
      (function(d) {
        var wf = d.createElement('script'), s = d.scripts[0];
        wf.src = site.template + '/js/head.js';
        // wf.async = true;
        s.parentNode.insertBefore(wf, s);
        wf.onload = function(){
          
          // All JS
          head.load([
            site.template + '/bower_components/swiper/dist/js/swiper.min.js',
            site.template + '/fonts/fonts.css',
            site.template + '/js/polyfill.min.js',
            site.template + '/js/scripts.min.js',
            ], function(){
              // When files are loaded, load these ones
              // head.load( site.template + '/js/myfile.js' )
          })
          
          /* 
          Optional load JS conditional
          if(site.post_name === 'about'){
            head.load(
              site.template + '/js/box-2column-parallax.min.js'
            )
          }
          */
        }
      })(document);
    </script>

  </body>
</html>
<?php 
  $highlights = get_field( 'highlights' );
  if($highlights):
?>
<section class="content-wrapper pt6 pb6">

  <div class="flex-3 flex-gap-3 flex-intro">
    <?php foreach($highlights['list'] as $item): ?>
    <div class="item text-center">
      <img class="el-image" src="<?php echo $item['image']['url']; ?>" alt="">
      <h2 class="alternate pt1"><?php echo $item['title']; ?></h2>
      <div class="entry-content pt1">
        <?php echo apply_filters( 'the_content', $item['description'] ); ?>
      </div>
    </div>
    <?php endforeach; ?>
  </div>
</section>
<?php endif;
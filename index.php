<?php 
	get_header(); 
	the_post();
  $hero = get_field( 'hero_content' );
?>

  <div class="hero-wrapper">
    <div class="hero-content white">
      <div class="content-750 pt4">
        <h1 class="title-intro uppercase"><?php echo $hero['heading']; ?></h1>  
        <div class="pt1"><?php echo apply_filters( 'the_content', $hero['subheading'] ) ?></div>
        <a class="btn btn-big btn-white mt3" href="<?php echo $hero['btn_link']; ?>"><?php echo $hero['btn_text']; ?></a>
        
      </div>
    </div>
    <div class="hero-bg" style="background-image:url('<?php echo $hero['image']['url']; ?>');"></div>
  </div>

  <?php include 'template-home-intro.php' ?>
  <?php include 'template-home-swiper.php' ?>
  <?php include 'template-home-profiles.php' ?>
  <?php include 'template-home-products.php'; ?>
  <?php include 'template-home-newsletter.php'; ?>

<?php get_footer(); ?>
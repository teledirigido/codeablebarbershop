<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=0.8, maximum-scale=0.8">
    <meta name="author" content="Miguel Garrido">
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-XXXXXXX-X"></script> -->
    <?php wp_head(); ?>
  </head>
  <body>
    <script>
      if (window.location.host==="domain.com" || window.location.host==="www.domain.com") {
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-XXXXXXX-X');
      }
    </script>

    <!--[if lte IE 9]>
      <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
    <![endif]-->
    <div class="header-wrapper">
      <div class="trigger-wrapper">
        <a class="header-mobile-trigger" href="#">
          <span></span>
        </a>
      </div>
      
      <nav>
        <ul class="ul-horz">
          <li><a href="<?php bloginfo('url') ?>/">Home</a></li>
          <li><a href="<?php bloginfo('url') ?>/about">About</a></li>
          <li class="logo"><a href="<?php bloginfo('url') ?>"><img src="<?php echo get_bloginfo( 'template_url' ) ?>/images/logo_long.png" alt=""></a></li>
          <li><a href="<?php bloginfo('url') ?>/contact">Contact</a></li>
          <li><a href="<?php bloginfo('url') ?>/store">Store</a></li>
        </ul>
      </nav>
    </div>
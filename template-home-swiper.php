<?php 
  $slider = get_field( 'slider' );
?>
<h2 class="title-intro title-before text-center mb5"><?php echo $slider['title'] ?></h2>
<div class="swiper-home-intro swiper-container">
  <div class="swiper-wrapper">
    <?php foreach($slider['slider'] as $item): ?>
    <div class="swiper-slide item">
      <img src="<?php echo $item['image']['url']; ?>" alt="">
    </div>
    <?php endforeach; ?>
  </div>
  <div class="swiper-button swiper-button-prev"></div>
  <div class="swiper-button swiper-button-next"></div>
  <div class="swiper-scrollbar-wrapper">
    <div class="swiper-scrollbar"></div>  
  </div>
</div>